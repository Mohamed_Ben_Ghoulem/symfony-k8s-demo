function FindProxyForURL(url, host) {
    if (host.includes('sfday.demo')){
        return 'PROXY 34.66.116.248:80';
    }
    // All other domains should connect directly without a proxy
    return "DIRECT";
}
