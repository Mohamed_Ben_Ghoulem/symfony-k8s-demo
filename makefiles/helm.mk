helm-lint:
	@docker-compose run --rm helm /bin/sh -c "helm lint /var/www/sfday/charts/SFday-demo"

helm-template: helm-template-out-prod helm-template-out-staging helm-template-out-review

helm-template-out-prod:
	rm -rf k8s/prod/*
	@docker-compose run --rm helm /bin/sh -c "helm template /var/www/sfday/charts/SFday-demo/ \
		--output-dir /var/www/sfday/k8s/prod \
        --set app.gitlab.projectPath="project/path" \
	    --set app.env=prod \
	    --set app.tag=prod-abcd1234 \
	    --set app.url=https://prod.example.com/ \
	    --set gitlabRegistrySecret=secret-sfday-prod \
	    --set php.image=registry.com/myproject/php \
	    --set nginx.image=registry.com/myproject/nginx"

helm-template-out-staging:
	rm -rf k8s/staging/*
	@docker-compose run --rm helm /bin/sh -c "helm template /var/www/sfday/charts/SFday-demo/ \
		--output-dir /var/www/sfday/k8s/staging \
        --set app.gitlab.projectPath="project/path" \
	    --set app.env=staging \
	    --set app.tag=staging-abcd1234 \
	    --set app.url=https://staging.example.com/ \
	    --set gitlabRegistrySecret=secret-sfday-staging \
	    --set php.image=registry.com/myproject/php \
	    --set nginx.image=registry.com/myproject/nginx"

helm-template-out-review:
	rm -rf k8s/review/*
	@docker-compose run --rm helm /bin/sh -c "helm template /var/www/sfday/charts/SFday-demo/ \
		--output-dir /var/www/sfday/k8s/review \
        --set app.gitlab.projectPath="project/path" \
	    --set app.env=review-feature-branch \
	    --set app.tag=review-feature-abcd1234 \
	    --set app.url=https://review-feature-branch.example.com/ \
	    --set gitlabRegistrySecret=secret-sfday-review \
	    --set php.image=registry.com/myproject/php \
	    --set nginx.image=registry.com/myproject/nginx"
