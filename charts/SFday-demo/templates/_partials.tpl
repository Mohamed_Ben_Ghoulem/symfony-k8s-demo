{{- define "sfday.resources" -}}
resources:
  requests:
    memory: "{{ .requests.memory }}"
    cpu: "{{ .requests.cpu }}"
  limits:
    memory: "{{ .limits.memory }}"
    cpu: "{{ .limits.cpu }}"
{{- end -}}
{{- define "sfday.database.credentials" -}}
- name: DATABASE_USER
  valueFrom:
    secretKeyRef:
      name: sfday-database
      key: user
- name: DATABASE_PASSWORD
  valueFrom:
    secretKeyRef:
      name: sfday-database
      key: password
{{- end -}}
{{- define "sfday.default_labels" -}}
app: {{ $.Values.app.name }}
environment: {{ $.Values.app.env }}
{{- end -}}
